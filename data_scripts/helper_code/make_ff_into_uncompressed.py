"""
Create an uncompressed FF file, using mule.

Core code originally from sworsley.

Called from "./make_ff_into_uncompressed.sh"
See there for essential calling environment setup.

"""

import mule

import os
from pathlib import Path

_FF_PATHNAME = 'FF_PATHNAME'

ff_pathname = str(os.environ.get(_FF_PATHNAME, None))
if not Path(ff_pathname).is_file():
    msg = (f'The FF does not exist: "${_FF_PATHNAME}" must be defined as a valid file.')
    raise(ValueError(msg))


in_path = ff_pathname
out_path = ff_pathname + '.uncompressed'

ff = mule.FieldsFile.from_file(in_path)
ff_out = ff.copy()

for field in ff.fields:
    if field.lbrel in (2,3):
        dp = mule.ArrayDataProvider(field.get_data())
        new_field = field.copy()
        new_field.set_data_provider(dp)
        new_field.lbpack = 0
        new_field.bacc = 0
        ff_out.fields.append(new_field)

print(f"Saving file: {out_path}")
ff_out.to_file(out_path)
