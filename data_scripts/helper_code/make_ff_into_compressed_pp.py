"""
Take the compressed standard FF data file, and save it as a compressed PP file.

Note: use mule, because Iris save of compressed PP fields seems to be broken
(at Iris 2.4).

"""
import mule
from mule.pp import fields_to_pp_file

import os
from pathlib import Path

_FF_PATHNAME = 'FF_PATHNAME'

ff_pathname = str(os.environ.get(_FF_PATHNAME, None))
if not Path(ff_pathname).is_file():
    msg = (f'The FF does not exist: "${_FF_PATHNAME}" must be defined as a valid file.')
    raise(ValueError(msg))


in_path = ff_pathname
out_path = ff_pathname + '.compressed.pp'

ff = mule.FieldsFile.from_file(in_path)

print(f"Saving file: {out_path}")
fields_to_pp_file(out_path, ff.fields)
