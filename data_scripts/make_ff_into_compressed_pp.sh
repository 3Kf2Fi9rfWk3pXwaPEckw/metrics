#!/usr/bin/bash

# Create a compressed PP file, using mule.
# This shell script exists to capture how we have to run with scitools+mule.

# Loading with scitools + mule, should probably be something like..
#     $ module load scitools/experimental-current
#     $ module load um_tools/next
#
# As-of 2020-02-03, that does not work, but the following does ...

set -eu

module load scitools/experimental-current
module load um_tools/2019.01.1

# Export the location of the (compressed) FieldsFile, e.g.
export FF_PATHNAME=/project/avd/ng-vat/data/performance_test_data/umglaa_pa000-theta

# NB: The output uncompressed file will be saved to the same directory as the input FF
python helper_code/make_ff_into_compressed_pp.py
