"""
Take the compressed standard FF data file, and save is as a NetCDF file.

"""

import os
from pathlib import Path

import iris

_FF_PATHNAME = 'FF_PATHNAME'

ff_pathname = Path(os.environ.get(_FF_PATHNAME, ""))
if not ff_pathname.is_file():
    msg = (f'The FF does not exist: "${_FF_PATHNAME}" must be defined as a valid file.')
    raise(ValueError(msg))


in_path = ff_pathname
out_path = ff_pathname.with_suffix(".nc")

cube_list = iris.load(str(in_path))

print(f"Saving file: {out_path}")
iris.save(cube_list, out_path)
