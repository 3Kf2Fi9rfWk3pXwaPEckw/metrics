"""
Called at start of ASV's build command (see asv.conf.json).
Checks for changes in dependency specification since the last benchmarked
commit, updates environment if necessary.
"""

from hashlib import sha256
import json
from os import chdir
from pathlib import Path
from shutil import rmtree
from subprocess import run, TimeoutExpired
from sys import argv
from urllib.request import urlretrieve
from zipfile import ZipFile


ENV_DIR = Path(argv[1])
REPO_DIR = Path(argv[2])


def _conda_env_update(env_spec_path: Path):
    conda_command = f"conda env update -p '{ENV_DIR}' -f '{env_spec_path}'"
    try:
        run(conda_command, shell=True, timeout=900)
    except TimeoutExpired:
        raise TimeoutError(f"Conda installation exceeded 15mins - implies "
                           f"a problem. Consider deleting {ENV_DIR}, then "
                           f"restarting benchmarking at this commit.")


def _install_iris(iris_commit):
    iris_dir = ENV_DIR / "iris"
    if iris_dir.is_dir():
        rmtree(iris_dir)

    # Download Iris.
    github_archive_url = (f"https://github.com/SciTools/iris/archive/{iris_commit}.zip")
    iris_zip = Path(urlretrieve(github_archive_url, "iris.zip")[0])
    with ZipFile(iris_zip, "r") as zip_open:
        zip_open.extractall()
    Path(f"iris-{iris_commit}").rename(iris_dir)
    iris_zip.unlink()

    # Install Iris dependencies.
    # Symlink not preserved from GitHub zip, so needs manual handling.
    env_spec_iris_symlink = iris_dir / "requirements" / "ci" / "iris.yml"
    with env_spec_iris_symlink.open() as file:
        env_spec_iris_name = file.read().strip()
    env_spec_iris = env_spec_iris_symlink.with_name(env_spec_iris_name)
    _conda_env_update(env_spec_iris)

    # Optional configuration of Iris. Don't currently need anything.
    site_cfg_content = [
        "[Resources]",
        "[System]",
    ]
    site_cfg_path = iris_dir / "lib" / "iris" / "etc" / "site.cfg"
    with site_cfg_path.open("w+") as site_cfg:
        site_cfg.writelines(line + "\n" for line in site_cfg_content)

    # Install Iris.
    chdir(iris_dir)
    install_command = "python setup.py install"
    run(install_command, shell=True)


def main():
    # Locate dependency information.
    env_spec_path = REPO_DIR / "requirements" / "ci" / "iris-ugrid.yml"
    iris_commit_path = REPO_DIR / "requirements" / "manual" / "iris_commit.txt"
    with iris_commit_path.open() as file:
        iris_commit = file.read().strip()

    # Store dependency cache info for downstream use.
    new_cache = {"iris_commit": iris_commit}
    with env_spec_path.open("rb") as file:
        new_cache["env_spec"] = sha256(file.read()).hexdigest()

    cache_dir = ENV_DIR / "env_info_cache"
    # Make the directory if it doesn't exist already.
    cache_dir.mkdir(parents=True, exist_ok=True)
    cache_path = cache_dir / "env_info.json"
    cache_path.touch(exist_ok=True)

    # Check if environment already has correct dependencies.
    with cache_path.open("r") as file:
        try:
            env_ok = new_cache == json.load(file)
        except json.JSONDecodeError:
            env_ok = False

    # Check if the environment already contains the required dependencies.
    if not env_ok:
        # Install the correct dependencies into the environment.
        _install_iris(iris_commit)
        # Install iris-ugrid dependencies.
        _conda_env_update(env_spec_path)
        # Record the dependencies that have been installed.
        with cache_path.open("w") as file:
            file.write(json.dumps(new_cache))


if __name__ == "__main__":
    main()
