"""Common code for benchmarks."""

import os
from pathlib import Path

# Environment variable names
_ASVDIR_VARNAME = 'ASV_DIR'  # As set in nightly script "asv_nightly/asv.sh"
_DATADIR_VARNAME = 'BENCHMARK_DATA'  # For local runs

ARTIFICIAL_DIM_SIZE = int(10e3)  # For all artificial cubes, coords etc.

# Work out where the benchmark data dir is.
asv_dir = os.environ.get('ASV_DIR', None)
if asv_dir:
    # For an overnight run, this comes from the 'ASV_DIR' setting.
    benchmark_data_dir = Path(asv_dir) / 'data'
else:
    # For a local run, you set 'BENCHMARK_DATA'.
    benchmark_data_dir = os.environ.get(_DATADIR_VARNAME, None)
    if benchmark_data_dir is not None:
        benchmark_data_dir = Path(benchmark_data_dir)


def testdata_path(*path_names):
    """
    Return the path of a benchmark test data file.

    These are based from a test-data location dir, which is either
    ${}/data (for overnight tests), or ${} for local testing.

    If neither of these were set, an error is raised.

    """.format(_ASVDIR_VARNAME, _DATADIR_VARNAME)
    if benchmark_data_dir is None:
        msg = ('Benchmark data dir is not defined : '
               'Either "${}" or "${}" must be set.')
        raise(ValueError(msg.format(_ASVDIR_VARNAME, _DATADIR_VARNAME)))
    path = benchmark_data_dir.joinpath(*path_names)
    path = str(path)  # Because Iris doesn't understand Path objects yet.
    return path


def disable_repeat_between_setup(benchmark_object):
    """
    Decorator for benchmarks where object persistence would be inappropriate.

    E.g:
        * Benchmarking data realisation
        * Benchmarking Cube coord addition

    Can be applied to benchmark classes/methods/functions.

    https://asv.readthedocs.io/en/stable/benchmarks.html#timing-benchmarks

    """
    # Prevent repeat runs between setup() runs - object(s) will persist after 1st.
    benchmark_object.number = 1
    # Compensate for reduced certainty by increasing number of repeats.
    #  (setup() is run between each repeat).
    #  Minimum 5 repeats, run up to 30 repeats / 20 secs whichever comes first.
    benchmark_object.repeat = (5, 30, 20.)
    # ASV uses warmup to estimate benchmark time before planning the real run.
    #  Prevent this, since object(s) will persist after first warmup run,
    #  which would give ASV misleading info (warmups ignore ``number``).
    benchmark_object.warmup_time = 0.

    return benchmark_object
