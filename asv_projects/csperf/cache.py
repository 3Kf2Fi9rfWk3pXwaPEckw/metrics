import argparse
import codecs
from datetime import datetime
from hashlib import sha256
import json
from pathlib import Path
from subprocess import check_call, check_output


#: nox/ASV cache filename.
CACHE_FILE = "cache.json"

#: Cache key for active nox session metric.
FLAG_ACTIVE = "active"

#: Look-up table to control package building, installation and caching.
lookup = {
    "iris": {
        "archive": "https://github.com/SciTools/iris/archive",
        "arg": {
            "commit": None,
            "src-dir": None,
        },
        "flag": {
            "commit": "iris-commit",
            "sha-build": "iris-sha-build",
        },
    },
}


def log(message, new=False):
    """
    Simple logger for added a log entry to a log file.

    Parameters
    ----------
    message : str
        The message to be captured and logged.
    new : bool
        If True removes existing log file before
        creating a new log file. Default is False.

    """
    log = conf_dir / "log.txt"
    if new:
        if log.is_file():
            log.unlink()
    with open(log, "a") as fo:
        fo.write(f"{datetime.now()}: {message}\n")


def python_version():
    """
    Determine the version of Python within the ASV environment.

    Returns
    -------
    tuple of str, str
        The full python version and the version major.minor

    """
    command = ["python", "--version"]
    full = check_output(command).decode("utf-8").strip()
    version = full.split()[1].split(".")
    major_minor = f"{version[0]}{version[1]}"
    return full, major_minor


def requirements(base_dir):
    """
    Determine the name and sha256sum of the associated package
    requirements file.

    The associated ASV environemnt Python version determines
    the package requirement file that is read.

    Parameters
    ----------
    base_dir : Path
        The root directory containing the requirements file.

    Returns
    -------
    tuple of str, Path
        The requirements sha and location.

    """
    log("\t-- requirements --")
    full, major_minor = python_version()
    log(f"\t\t{full}")
    fname = (base_dir / "requirements" / "ci" / f"py{major_minor}.yml").resolve(
        strict=True
    )
    with open(fname, "rb") as fi:
        sha = sha256(fi.read()).hexdigest()
    log("\t-- end --")
    return sha, fname


def load_cache(base_dir):
    """
    Read the nox/ASV cache file.

    Parameters
    ----------
    base_dir : Path
        The root directory containing the nox/ASV cache.

    Returns
    -------
    dict
        The nox/ASV cache.

    """
    fname = (Path(base_dir) / CACHE_FILE).resolve(strict=True)
    with open(fname, "rb") as fi:
        cache = json.load(codecs.getreader("utf-8")(fi))
    return cache


def dump_cache(base_dir, cache):
    """
    Write the nox/ASV cache, encoding as a json file.

    Parameters
    ----------
    base_dir : Path
        The root directory to contain the nox/ASV cache file.
    cache : dict
        The nox/ASV cache to be written.

    """
    fname = (Path(base_dir) / CACHE_FILE).resolve(strict=True)
    with open(fname, "wb") as fo:
        json.dump(cache, codecs.getwriter("utf-8")(fo), indent=4, sort_keys=True)


def process(package, cache, env_dir):
    """
    Manage the build and/or installation of the package
    within the associated ASV environment.

    Parameters
    ----------
    package : dict
        The relevant section of the look-up table for
        a specific package.
    cache : dict
        The nox/ASV cache.
    env_dir : Path
        The ASV environment root directory.
    asv_install : bool
        Whether ASV is performing a build or install
        of the specified package.

    """
    log("-- process --")
    _, major_minor = python_version()
    flag = package["flag"]["sha-build"]
    actual, fname = requirements(package["arg"]["src-dir"])
    log(f"\trequirements={fname}")
    log(f"\tactual={actual}")
    versions = cache.get(flag)
    log(f"\tversions={versions}")
    expected = None
    if versions is not None:
        expected = versions.get(major_minor)
    log(f"\texpected={expected}")

    # Determine whether the package requirements have changed, or the
    # environment is new, thus requiring an environment update.
    install_status_file = Path(env_dir) / "asv-install-status.json"
    with open(install_status_file, "rb") as file:
        asv_install_status = json.load(codecs.getreader("utf-8")(file))
    env_new = asv_install_status["commit_hash"] is None
    requirements_changed = actual != expected
    update = requirements_changed or env_new
    log(f"\tupdate={update}")

    if update:
        command = ["conda", "env", "update", f"--prefix={env_dir}", f"--file={fname}"]
        check_call(command)

    # Cache the requiements sha, based on the ASV environment
    # Python version.
    if versions is None:
        cache[flag] = {major_minor: actual}
    else:
        versions[major_minor] = actual
        cache[flag] = versions

    log("-- end --\n")


################################################################################

# Parse the CLAs.
parser = argparse.ArgumentParser(description="asv to nox cache manager")
parser.add_argument("--build_dir", "-b", action="store", help="asv build directory")
parser.add_argument(
    "--conf_dir", "-c", action="store", help="asv configuration directory"
)
parser.add_argument(
    "--env_dir", "-e", action="store", help="asv current active environment root"
)
parser.add_argument(
    "--commit", action="store", help="git commit sha of asv currently installed project"
)
parser.add_argument("--install", action="store_true", help="asv install")
args = parser.parse_args()

# ASV controlled state, see associated asv.conf.json
build_dir = Path(args.build_dir).resolve(strict=True)
conf_dir = Path(args.conf_dir).resolve(strict=True)
env_dir = Path(args.env_dir).resolve(strict=True)
asv_commit = args.commit
asv_install = args.install
asv_build = not asv_install

cache = load_cache(conf_dir)
metric = cache[FLAG_ACTIVE]

# Cache the ASV derived commit sha for Iris.
cache["asv-commit"] = asv_commit

# Capture relevant state in log.
log("[START]", new=asv_build)
log(f"asv_commit={asv_commit}")
log(f"asv_install={asv_install}")
log(f"asv_build={asv_build}")
log(f"metric={metric}")

# Determine the packages that are being cached.
packages = sorted(lookup.keys())

# Populate the commit sha of each package in the look-up table from the cache.
for package in packages:
    lookup[package]["arg"]["commit"] = cache[lookup[package]["flag"]["commit"]]

# Populate the src directory of each package in the look-up table.
for package in packages:
    src_dir = build_dir
    lookup[package]["arg"]["src-dir"] = src_dir

# Process the package for the given ASV command.
for package in packages:
    log(f"package={package}")
    process(lookup[package], cache, env_dir)

# Cache the associated ASV root environment directory for the metric.
cache[metric] = str(env_dir)

# Write the cache.
dump_cache(conf_dir, cache)

log("[END]\n")
