"""Common code for benchmarks."""
import os
from pathlib import Path

from iris.tests.stock.netcdf import (
    create_file__xios_2d_face_half_levels,
    create_file__xios_3d_face_half_levels,
)


# Get the common directory used for synthetic data testfiles.
_SYNTHETIC_DATA_PATH = Path(
    os.environ.get("NGVAT_SYNTHETIC_DATA_CACHEDIR", "invalid-path")
).resolve()
if not _SYNTHETIC_DATA_PATH.exists() or not _SYNTHETIC_DATA_PATH.is_dir():
    raise ValueError(
        "Please set $NGVAT_SYNTHETIC_DATA_CACHEDIR to a valid path :\n"
        "   'export NGVAT_SYNTHETIC_DATA_CACHEDIR=<synthetic-data directory>'."
    )


def make_cubesphere_testfile(c_size, n_levels=0, n_times=1):
    """
    Build a C<c_size> cubesphere testfile in a given directory, with a standard naming.
    If n_levels > 0 specified: 3d file with the specified number of levels.
    Skip the actual build, if a file of the appropriate name already exists.
    Return the file path.

    """
    global _SYNTHETIC_DATA_PATH
    n_faces = 6 * c_size * c_size
    stem_name = f"mesh_cubesphere_C{c_size}_t{n_times}"
    three_d = n_levels > 0
    if three_d:
        stem_name += f"_{n_levels}levels"
        func = create_file__xios_3d_face_half_levels
    else:
        func = create_file__xios_2d_face_half_levels
    file_path = (_SYNTHETIC_DATA_PATH / stem_name).with_suffix(".nc")
    if not file_path.exists():
        print(f"Creating new synthetic cubesphere file : {file_path}")
        kwargs = dict(
            temp_file_dir=str(file_path.parent),  # N.B. function is not Path-aware.
            dataset_name=stem_name,  # N.B. function adds the ".nc" extension
            n_times=n_times,
            n_faces=n_faces,
        )
        if three_d:
            kwargs["n_levels"] = n_levels
        new_path = func(**kwargs)
        if new_path != str(file_path) or not file_path.exists():
            raise ValueError("Failed to create file {file_path}")
    return file_path
