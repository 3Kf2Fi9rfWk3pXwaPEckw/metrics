from pathlib import Path

from iris import load_cube

# TODO: remove uses of PARSE_UGRID_ON_LOAD once UGRID parsing is core behaviour.
from iris.experimental.ugrid import PARSE_UGRID_ON_LOAD

from benchmarks import make_cubesphere_testfile


# moo ls moose:/adhoc/projects/avd/asv/data_for_nightly_tests/
_DATA_DIR = Path("/data/local/avd/asv/data/cperf")


def loadargs_single_diagnostic_UM(format=None, three_d=False, three_times=False):
    """
    formats:
        * None = packed FF (WGDOS lbpack = 1)
        * uncompressed = unpacked FF (lbpack = 0)
        * nc = UM data loaded into Iris saved out as NetCDF file

    three_d:
        * False = surface_temp diagnostic, N1280 file (1920, 2560)
        * True = theta diagnostic, N1280 file w/ 71 levels (1920, 2560, 71)

    three_times:
        * False = pa/pb000 files
        * True = pa/pb003 files

    """
    if three_times:
        numeric = "003"
    else:
        numeric = "000"

    if three_d:
        file_name = f"umglaa_pb{numeric}-theta"
    else:
        file_name = f"umglaa_pa{numeric}-surfacetemp"

    if format is None:
        pass
    elif format in ("uncompressed", "nc"):
        file_name += f".{format}"
    else:
        ValueError(f"Unhandled format: {format} .")

    file_path = str(_DATA_DIR / file_name)
    args = [file_path]
    kwargs = {}
    return args, kwargs


def create_umsized_synthetic_datafile(diagnostic, n_times=1):
    """
    Create an LFRic-style NetCDF file sized to be equivalent to a UM file
    containing a single diagnostic (the ``diagnostic`` argument).

    """
    # The data of the UM file noted above has dtype=np.float32 shape=(1920, 2560)
    # The closest cubesphere size in terms of datapoints is sqrt(1920*2560 / 6) ~= 905
    # I.E. "C905"
    cells_per_panel_edge = 905
    kwargs = dict(c_size=cells_per_panel_edge, n_times=n_times)

    if diagnostic == "surfacetemp":
        pass
    elif diagnostic == "theta":
        kwargs["n_levels"] = 71
    else:
        raise ValueError(f"Unhandled diagnostic: {diagnostic} .")

    file_path = make_cubesphere_testfile(**kwargs)
    return str(file_path)


def loadargs_single_diagnostic_LFRic(three_d=False, three_times=False):
    if three_d:
        diagnostic = "theta"
    else:
        diagnostic = "surfacetemp"

    if three_times:
        n_times = 3
    else:
        n_times = 1

    file_path = create_umsized_synthetic_datafile(diagnostic, n_times)
    args = [file_path]
    kwargs = {}
    return args, kwargs


# Actual benchmarks


class LoadTypesMixin:
    """
    Basic functions to test loading 2d or 3d data (2d default).
    Parametrised by file type.

    """
    three_d = False
    three_times = False

    params = ["LFRic", "UM", "UM_lbpack0", "UM_netcdf"]
    param_names = ["file type"]

    def setup(self, file_type):
        # LFRic
        self.loadargs_lfric = loadargs_single_diagnostic_LFRic(
            three_d=self.three_d, three_times=self.three_times
        )
        # UM
        self.loadargs_um = loadargs_single_diagnostic_UM(
            three_d=self.three_d, three_times=self.three_times
        )
        self.loadargs_um_lbpack0 = loadargs_single_diagnostic_UM(
            "uncompressed", three_d=self.three_d, three_times=self.three_times
        )
        self.loadargs_um_netcdf = loadargs_single_diagnostic_UM(
            "nc", three_d=self.three_d, three_times=self.three_times
        )

        self.file_type_lookup = {
            "LFRic": self.load_LFRic,
            "UM": self.load_UM,
            "UM_lbpack0": self.load_UM_lbpack0,
            "UM_netcdf": self.load_UM_netcdf,
        }

    def load_LFRic(self):
        args, kwargs = self.loadargs_lfric
        with PARSE_UGRID_ON_LOAD.context():
            return load_cube(*args, **kwargs)

    def load_UM(self):
        args, kwargs = self.loadargs_um
        return load_cube(*args, **kwargs)

    def load_UM_lbpack0(self):
        args, kwargs = self.loadargs_um_lbpack0
        return load_cube(*args, **kwargs)

    def load_UM_netcdf(self):
        args, kwargs = self.loadargs_um_netcdf
        return load_cube(*args, **kwargs)


class Single2d_LoadCube(LoadTypesMixin):
    def __init__(self):
        # See notes in Single2d_RealiseCube class.
        for att, val in {"number": 1, "warmup_time": 0.0}.items():
            setattr(Single2d_LoadCube.time_cperf_load_with_realised_coords, att, val)

    def load_common(self, file_type, realise_coords):
        load_func = self.file_type_lookup[file_type]
        cube = load_func()
        assert cube.has_lazy_data()

        # UM files load lon/lat as DimCoords, which are always realised.
        expecting_lazy_coords = file_type == "LFRic"
        for coord_name in "longitude", "latitude":
            coord = cube.coord(coord_name)
            assert coord.has_lazy_points() == expecting_lazy_coords
            assert coord.has_lazy_bounds() == expecting_lazy_coords
            if realise_coords:
                _ = coord.points, coord.bounds
                assert not coord.has_lazy_points()
                assert not coord.has_lazy_bounds()

    def time_cperf_load(self, file_type):
        """
        The 'real world comparison'
          * UM coords are always realised (DimCoords).
          * LFRic coords are not realised by default (MeshCoords).

        """
        self.load_common(file_type, realise_coords=False)

    def time_cperf_load_with_realised_coords(self, file_type):
        """A valuable extra comparison where both UM and LFRic coords are realised."""
        self.load_common(file_type, realise_coords=True)


class Single2d_RealiseCube(LoadTypesMixin):
    # Prevent multiple benchmark calls within a timing iteration (aka a "repeat").
    # Because the benchmark realizes the cube, and needs it to be lazy to start with (which it checks).
    # To ensure this, we must turn off the warmup phase, and set number=1
    number = 1
    warmup_time = 0.0

    def setup(self, file_type):
        super().setup(file_type)
        load_func = self.file_type_lookup[file_type]
        self.cube = load_func()

    def time_cperf_realise(self, file_type):
        assert self.cube.has_lazy_data()
        _ = self.cube.data
        assert not self.cube.has_lazy_data()


class LoadAdjust_3d_Mixin:
    """
    Mixin to convert 2d benchmark classes load 3d data.
    Note: must be imported *before* the base class, to override the 'three_d' setting.
    """
    three_d = True
    timeout = 600.0


class Single3d_LoadCube(LoadAdjust_3d_Mixin, Single2d_LoadCube):
    "3d-version load : NB mixin must be _first_ in import list."
    pass


class Single3d_RealiseCube(LoadAdjust_3d_Mixin, Single2d_RealiseCube):
    "3d-version realise : NB mixin must be _first_ in import list."
    pass


class LoadAdjust_3Times_Mixin:
    """
    Mixin to convert 1-time benchmark classes to load 3-time data.
    Note: must be imported *before* the base class, to override the 'three_times' setting.
    """
    three_times = True


class Single3Times_LoadCube(LoadAdjust_3Times_Mixin, Single2d_LoadCube):
    """3-time-version load : NB mixin must be _first_ in import list."""


class Single3Times_RealiseCube(LoadAdjust_3Times_Mixin, Single2d_RealiseCube):
    """3-time-version realise : NB mixin must be _first_ in import list."""
    pass
