from iris import load_cube

# TODO: remove uses of PARSE_UGRID_ON_LOAD once UGRID parsing is core behaviour.
from iris.experimental.ugrid import PARSE_UGRID_ON_LOAD

from benchmarks import make_cubesphere_testfile


# TODO: change to use multiple parameters in a single class - easier to expand
#  - pending support in root/noxfile.py.
class LoadMixin:
    # Parameterise by cube sizes.
    # Numbers are in "C" notation, (e.g. C1 is 6 faces, 8 nodes)
    params = [12, 384, 640, 960, 1280, 1668]

    # Make readable param names.
    param_names = ["cubesphere_C<N>"]

    def setup(self, param):
        file_kwargs = self.file_kwargs(param)
        self.file_path = make_cubesphere_testfile(**file_kwargs)

    def file_kwargs(self, param):
        return dict(c_size=param)

    def load_cube(self):
        with PARSE_UGRID_ON_LOAD.context():
            return load_cube(str(self.file_path))


class Sperf_2d_Cubesphere_Load(LoadMixin):
    def time_load_cube(self, param):
        cube = self.load_cube()
        assert cube.has_lazy_data()


class Sperf_2d_Cubesphere_Realise(LoadMixin):
    # Prevent multiple benchmark calls within a timing iteration (aka a "repeat").
    # Because the benchmark realizes the cube, and needs it to be lazy to start with (which it checks).
    # To ensure this, we must turn off the warmup phase, and set number=1
    number = 1
    warmup_time = 0.0

    def setup(self, param):
        super().setup(param)
        self.cube = self.load_cube()

    def time_realise_cube(self, param):
        assert self.cube.has_lazy_data()
        _ = self.cube.data
        assert not self.cube.has_lazy_data()

########

class LoadAdjust_3d_Mixin:
    """
    Mixin to convert 2d benchmark classes to load 3d data (fixed to 72 levels).
    Note: must be imported *before* the base class, to override 'file_kwargs'.
    """
    timeout = 600.0

    def file_kwargs(self, param):
        result = super().file_kwargs(param)
        result['n_levels'] = 72
        return result


class Sperf_3d_Cubesphere_Load(LoadAdjust_3d_Mixin, Sperf_2d_Cubesphere_Load):
    # Same parameterisation as 2d, but with 3d geometry - fixed to 8 vertical levels.
    # NB mixin must be _first_ in import list.
    pass


class Sperf_3d_Cubesphere_Realise(LoadAdjust_3d_Mixin, Sperf_2d_Cubesphere_Realise):
    # Same parameterisation as 2d, but with 3d geometry - fixed to 8 vertical levels.
    # NB mixin must be _first_ in import list.
    pass

########

class LoadAdjust_Nlevels_Mixin:
    """
    Mixin to convert 2d benchmark classes to load 3d data on parametrised levels.
    Note: must be imported *before* the base class, to override the in-common attributes.
    """
    # Parameterise by level count.
    params = [1, 36, 72]
    param_names = ["Number of levels"]

    def file_kwargs(self, param):
        # NOTE: makes *no* reference to a 'parent' version, as that uses a
        # different parameterisation.
        return dict(c_size=384, n_levels=param)


class Sperf_Multilevel_Cubesphere_Load(LoadAdjust_Nlevels_Mixin, Sperf_2d_Cubesphere_Load):
    # Same parameterisation as 2d, but with 3d geometry on parametrised n-levels.
    # NB mixin must be _first_ in import list.
    pass


class Sperf_Multilevel_Cubesphere_Realise(LoadAdjust_Nlevels_Mixin, Sperf_2d_Cubesphere_Realise):
    # Same parameterisation as 2d, but with 3d geometry on parametrised n-levels.
    # NB mixin must be _first_ in import list.
    pass

########

class LoadAdjust_3Times_Mixin:
    """
    Mixin to convert 2d benchmark classes to load time series data (fixed to 8
    time steps). Note: must be imported *before* the base class, to override
    ``file_kwargs``.
    """
    def file_kwargs(self, param):
        result = super().file_kwargs(param)
        result["n_times"] = 8
        return result


class Sperf_2dTime_Cubesphere_Load(LoadAdjust_3Times_Mixin, Sperf_2d_Cubesphere_Load):
    # Same parametrisation as 2d, but with a time series - fixed to 8 time steps.
    # NB mixin must be _first_ in import list.
    pass


class Sperf_2dTime_Cubesphere_Realise(LoadAdjust_3Times_Mixin, Sperf_2d_Cubesphere_Realise):
    # Same parametrisation as 2d, but with a time series - fixed to 8 time steps.
    # NB mixin must be _first_ in import list.
    pass

########

class LoadAdjust_Ntimes_Mixin:
    """
    Mixin to convert 2d benchmark classes to parametrise on n-times rather than
    cubesphere size.
    Note: must be imported *before* the base class, to override the in-common
    attributes.
    """
    # Parametrise by n times. 3 for parity with CPerf, 72 as the maximum
    #  expected from LFRic.
    params = [1, 3, 36, 72]
    param_names = ["Number of time steps"]

    def file_kwargs(self, param):
        return dict(c_size=384, n_times=param)


class SPerf_Multitime_Cubesphere_Load(LoadAdjust_Ntimes_Mixin, Sperf_2d_Cubesphere_Load):
    # Parametrised to scale time steps instead of cubesphere size.
    # NB mixin must be _first_ in import list.
    pass


class SPerf_Multitime_Cubesphere_Realise(LoadAdjust_Ntimes_Mixin, Sperf_2d_Cubesphere_Realise):
    # Parametrised to scale time steps instead of cubesphere size.
    # NB mixin must be _first_ in import list.
    pass
