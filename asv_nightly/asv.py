"""
Script to interpret a list (asv_repos.yml) of ASV benchmark collections.

First acquires the latest iris-test-data for use in the benchmarks.
"""

from datetime import datetime
import feedparser
import git
import os
from pathlib import Path
from shutil import rmtree
from subprocess import run
from sys import stderr
from warnings import warn
import yaml


def manage_repo(name, source_url, target_dir):
    """Return a specified repo at a specified path, cloning first if necessary."""
    subdir = target_dir / name
    repo = None
    try:
        repo = git.Repo(subdir)
    except (git.InvalidGitRepositoryError, git.NoSuchPathError) as err:
        print(f"Could not find git repository: {subdir}, cloning afresh")
        if subdir.exists():
            rmtree(subdir)
        subdir.mkdir(parents=True)
        try:
            repo = git.Repo.clone_from(source_url, subdir)
        except git.exc.GitCommandError:
            warn(f"Error cloning {source_url}.")

    if repo:
        repo_url = repo.remotes.origin.url
        if repo_url != source_url:
            warn(f"Provided repo URL: {source_url} does not match existing "
                 f"URL: {repo_url}")
            repo = None
        else:
            # Avoid detached head state.
            repo.git.checkout(".")

    return repo, subdir


def skip_collection_warning(collection_dict, error_type):
    """Dedicated function for the various similar 'skipping collection' warnings."""
    collection_name = collection_dict["collection name"]
    asv_conf_dict = collection_dict["asv conf location"]
    repo = asv_conf_dict['repo']
    branch = asv_conf_dict['branch']
    subdir = asv_conf_dict['subdirectory']

    if error_type == "repo":
        warn_string = f"Upstream error identifying repo: {repo}."

    elif error_type == "branch":
        warn_string = f"Error checking out and pulling branch: '{branch}' in " \
                      f"repo: {repo}."

    elif error_type == "subdirectory":
        warn_string = f"Subdirectory not found: {subdir} in repo: {repo}, " \
                      f"branch: '{branch}'."

    warn_string += f"\nSkipping collection: {collection_name}"
    warn(warn_string)


asv_dir = Path(os.environ["ASV_DIR"])
results_dir = Path(os.environ["RESULTS_DIR"])
results_url = os.environ.get("RESULTS_URL")

# Fetch Iris Test Data, check out latest master.
test_data_repo, test_data_dir = \
    manage_repo("iris-test-data",
                "https://github.com/SciTools/iris-test-data.git",
                asv_dir)
test_data_repo.git.checkout("master")
test_data_repo.git.pull("origin")
os.environ["OVERRIDE_TEST_DATA_REPOSITORY"] = str(test_data_dir / "test_data")

# Read the repo list from the configuration file.
own_dirpath = Path(__file__).parent
yaml_path = own_dirpath / "asv_repos.yml"
with open(yaml_path, 'r') as stream:
    collection_list = yaml.load(stream, Loader=yaml.BaseLoader)
collections_dir = asv_dir / "benchmark_collections"

for collection_dict in collection_list:
    # Iterate through the listed collections.
    collection_name = collection_dict["collection name"]
    asv_conf_dict = collection_dict["asv conf location"]
    asv_conf_git = asv_conf_dict["repo"]
    repo, repo_subdir = manage_repo(collection_name,
                                    asv_conf_git,
                                    collections_dir)
    if repo is None:
        skip_collection_warning(collection_dict, error_type="repo")
        continue

    # Checkout each listed branch in the current repo.
    asv_conf_branch = asv_conf_dict["branch"]
    try:
        if asv_conf_branch in repo.branches:
            repo.git.checkout(asv_conf_branch)
            repo.git.pull("origin")
        else:
            repo.git.fetch()
            repo.git.checkout(asv_conf_branch)
    except git.exc.GitCommandError:
        skip_collection_warning(collection_dict, error_type="branch")
        continue

    asv_conf_dir = repo_subdir / asv_conf_dict["subdirectory"]
    try:
        os.chdir(asv_conf_dir)
    except FileNotFoundError:
        skip_collection_warning(collection_dict, error_type="subdirectory")
        continue

    # Create sub-directory for publishing the results from this collection.
    results_subdir = results_dir / collection_name
    if not results_subdir.exists():
        results_subdir.mkdir(parents=True)
    if results_url:
        results_suburl = f"{results_url}/{collection_name}"
    else:
        results_suburl = None

    # Perform "asv run" with each of the argument strings listed.
    run_list = collection_dict["asv run arguments"]
    run_datetime = datetime.now()
    for run_args in run_list:
        print(f"RUNNING collection: {collection_name}; "
              f"asv run args: {run_args}")
        run(f"asv run {run_args}", shell=True)

    # Publish all results for this collection.
    run(["asv", "publish", "--html-dir", results_subdir])

    regression_feed = feedparser.parse(results_subdir / "regressions.xml")
    for entry in regression_feed.entries:
        if entry.updated > run_datetime.isoformat():
            summary = entry.summary
            if results_suburl is not None:
                summary = entry.summary.replace("index.html",
                                                f"{results_suburl}/index.html")
            print(f"REGRESSION DETECTED: \n{entry.title}: \n{summary}\n", file=stderr)

    # Undo any changes that have been made to the repo.
    repo.git.checkout(".")


print("BENCHMARKING COMPLETE")
