#!/usr/bin/env bash

# Stop in case of an error.
set -e

if [[ "$HOSTNAME" = eldavdbuild02 ]]; then
    # The working directory, including the cached files (to reduce
    # download-induced runtime) and the Python environment.
    export ASV_DIR="/data/local/avd/asv"
    # The directory where the results will be published.
    export RESULTS_DIR="/data/users/avd/asv_publish"
    # The web address associated with the directory.
    export RESULTS_URL="https://www-avd/~avd/asv"
    OWN_DIR=$(dirname $0)
    # Conda environment containing requisite packages for the main script.
    source ${ASV_DIR}/miniconda3/bin/activate
    # Run main script but only output stderr (includes regressions).
    python ${OWN_DIR}/asv.py 2>&1 >/dev/null
else
    echo "Cancelling - will only run on eldavdbuild02."
    exit 1
fi
